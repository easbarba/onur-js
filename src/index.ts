/*
 *  onur-js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  onur-js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with onur-js. If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

import process from 'process';

import { all } from './files';

export const hello: string = 'Hello, World!';

const command: string = process.argv[2];

// RUN
if (command === '--grab') {
    await all().then((files) => {
        files.forEach((file) => {
            console.log(`- ${file}`);
        });
    });
}

if (command === '--help' || command === undefined) {
    console.log('onur - easily manage multiple floss repositories.');
}

// const file = Bun.file(import.meta.dir + "/package.json"); // BunFile
// const pkg = await file.json(); // BunFile extends Blob
// pkg.name = "my-package";
// pkg.version = "1.0.0";
